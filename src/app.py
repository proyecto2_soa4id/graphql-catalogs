from mongoengine import connect
from flask import Flask
from flask_graphql import GraphQLView
from schema import schema

default_port = 5000
connect('compratec_db', host='35.153.167.130') 
app = Flask(__name__)
app.debug = True

app.add_url_rule('/', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=default_port)