import graphene
from graphene_mongo import MongoengineConnectionField
from graphene.relay import Node
from models import Catalog, CatalogNode
from mutations import createCatalog, updateCatalogByType, deleteCatalog

class Query(graphene.ObjectType):
    all_catalogs = MongoengineConnectionField(CatalogNode)
    get_catalog = Node.Field(CatalogNode)

class Mutation(graphene.ObjectType):
    create_catalog = createCatalog.Field()
    update_catalog_by_type = updateCatalogByType.Field()
    delete_catalog = deleteCatalog.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)