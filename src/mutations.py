import graphene
from graphene.relay import Node
from models import Catalog, CatalogNode

#Create mutation
class createCatalog(graphene.Mutation):
    type = graphene.String(required=True)
    location = graphene.String(required=True)
    amount_products = graphene.Int(required=True)

    class Arguments:
        type = graphene.String(required=True)
        location = graphene.String(required=True)
        amount_products = graphene.Int(required=True)

    def mutate(self, info, type, location, amount_products):
        catalog = Catalog(type=type, location=location, amount_products=amount_products)
        catalog.save()

        return createCatalog(
            type=catalog.type,
            location=catalog.location,
            amount_products=catalog.amount_products
        )

#Update mutation   
class updateCatalogByType(graphene.Mutation):

    class Arguments:
        type = graphene.String(required=True)
        new_type = graphene.String(required=True)
    
    catalog = graphene.Field(CatalogNode)
    ok = graphene.Boolean()

    def mutate(self, info, type, new_type):
        tmp_catalog = Catalog.objects(type=type)
        if tmp_catalog:     
            tmp_catalog = tmp_catalog.get(type=type)
            tmp_catalog.type = new_type
            tmp_catalog.save()
        return updateCatalogByType(ok=True)
        
#Delete mutation 
class deleteCatalog(graphene.Mutation):

    class Arguments:
        type = graphene.String(required=True)

    catalog = graphene.Field(CatalogNode)
    ok = graphene.Boolean()

    def mutate(self, info, type):
        deleted = Catalog.objects(type=type)[0].delete()
        return deleteCatalog(ok=True)