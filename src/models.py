from datetime import datetime
from mongoengine import Document, EmbeddedDocument
from graphene_mongo import MongoengineObjectType
from graphene.relay import Node
from mongoengine.fields import (
    StringField, IntField
)

class Catalog(Document):
    meta = {'collection': 'catalog'}
    type = StringField()
    location = StringField()
    amount_products = IntField()

class CatalogNode(MongoengineObjectType):
    class Meta:
        model = Catalog
        interfaces = (Node,)